package com.flowkode.rie.server;

import com.flowkode.rie.util.RieException;
import com.flowkode.rie.util.SerializationUtils;

import javax.enterprise.inject.spi.CDI;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Optional;
import java.util.function.IntFunction;

/**
 * Created by ngraca on 12/29/16.
 */
public class RieHandler {

    private static class Holder {

        static final RieHandler INSTANCE = new RieHandler();
    }

    private RieHandler() {
    }

    public static RieHandler getInstance() {
        return Holder.INSTANCE;
    }

    public String executeMethodWithSerializedResult(String interfaceName, String methodName, String base64Arguments) {
        try {
            try {
                return SerializationUtils.serializeToBase64(executeMethod(interfaceName, methodName, base64Arguments));
            }
            catch (IOException e) {
                //wrap it
                throw new RieException(e);
            }
        }
        catch (RieException e) {
            try {
                //return it serialized
                return SerializationUtils.serializeToBase64(new RieException(e));
            }
            catch (IOException e1) {
                //no escape now we need to throw it
                throw new RuntimeException(e1);
            }
        }
    }

    private Object executeMethod(String interfaceName, String methodName, String base64Arguments) throws RieException {
        try {
            Object[] args;
            if (base64Arguments != null) {
                args = (Object[]) SerializationUtils.deserializeFromBase64(base64Arguments);
            }
            else {
                args = new Object[0];
            }

            return executeMethod(interfaceName, methodName, args);
        }
        catch (ClassNotFoundException e) {
            throw new RieException("Make sure your argument classes are in the classpath", e);
        }
        catch (IOException e) {
            throw new RieException(e);
        }
    }

    private Object executeMethod(String interfaceName, String methodName, Object[] args) throws RieException {
        try {
            Class<?> interfaceClass = Class.forName(interfaceName);

            final Class<?>[] parameterTypes = Arrays.stream(Optional.ofNullable(args).orElse(new Object[0]))
                                                    .map(Object::getClass)
                                                    .toArray((IntFunction<Class<?>[]>) Class[]::new);

            Object implClass = getImplementationClass(interfaceClass);

            Method method;
            switch (methodName) {
                case "equals":
                case "hashCode":
                case "toString":
                    method = implClass.getClass().getMethod(methodName, parameterTypes);
                    break;
                default:
                    method = interfaceClass.getMethod(methodName, parameterTypes);
            }

            return method.invoke(implClass, args);
        }
        catch (ClassNotFoundException e) {
            throw new RieException("Make sure your argument classes are in the classpath", e);
        }
        catch (NoSuchMethodException e) {
            throw new RieException("Method \"" + methodName + "\" was not found on " + interfaceName, e);
        }
        catch (IllegalAccessException e) {
            throw new RieException("Method \"" + methodName + "\" is not accessible on " + interfaceName, e);
        }
        catch (InvocationTargetException e) {
            throw new RieException(e.getCause());
        }
    }

    private Object getImplementationClass(Class<?> interfaceClass) throws RieException {
        //try CDI
        try {
            //do a forName to check for it
            Class.forName("javax.enterprise.inject.spi.CDI");
            return CDI.current().select(interfaceClass).get();
        }
        catch (ClassNotFoundException ignored) {
            //ignored
        }
        //try Spring
        if (SpringContextProvider.getApplicationContext() != null) {
            return SpringContextProvider.getApplicationContext().getBean(interfaceClass);
        }
        else {
            throw new RieException("Could not get the Implementation Class for: " + interfaceClass.getName());
        }
    }
}
