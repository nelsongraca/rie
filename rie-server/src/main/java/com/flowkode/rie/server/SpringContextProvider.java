package com.flowkode.rie.server;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * Created by ngraca on 1/16/17.
 */
@Component
public class SpringContextProvider {

    private static ApplicationContext context;

    public SpringContextProvider(ApplicationContext context) {
        SpringContextProvider.context = context;
    }

    public static ApplicationContext getApplicationContext() {
        return context;
    }

}
