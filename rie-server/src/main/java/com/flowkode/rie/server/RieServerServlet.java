package com.flowkode.rie.server;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by ngraca on 12/29/16.
 */
public class RieServerServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final PrintWriter writer = resp.getWriter();
        writer.write(RieHandler.getInstance().executeMethodWithSerializedResult(
                req.getParameter("interfaceName"),
                req.getParameter("methodName"),
                req.getParameter("arguments")
        ));
        writer.flush();
    }
}
