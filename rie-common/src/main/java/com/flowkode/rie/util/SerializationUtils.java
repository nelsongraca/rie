package com.flowkode.rie.util;

import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.Base64;

/**
 * Created by ngraca on 1/5/17.
 */
public final class SerializationUtils {

    public static final Object deserializeFromBase64(String content) throws ClassNotFoundException, IOException {
        byte[] bytes = Base64.getDecoder().decode(content);
        try (
                ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
                ObjectInputStream ois = new ObjectInputStream(bais)
        ) {
            return ois.readObject();
        }
    }

    public static final Object deserializeFromBase64(InputStream content) throws ClassNotFoundException, IOException {
        final byte[] src = IOUtils.toByteArray(content);
        byte[] bytes = Base64.getDecoder().decode(src);
        try (
                ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
                ObjectInputStream ois = new ObjectInputStream(bais)
        ) {
            return ois.readObject();
        }
    }

    public static final String serializeToBase64(Object obj) throws IOException {
        try (
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ObjectOutputStream oos = new ObjectOutputStream(baos)
        ) {
            oos.writeObject(obj);
            oos.flush();
            return Base64.getEncoder().encodeToString(baos.toByteArray());
        }
    }
}
