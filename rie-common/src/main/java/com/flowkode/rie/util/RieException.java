package com.flowkode.rie.util;

/**
 * Created by ngraca on 1/5/17.
 */
public class RieException extends Exception {

    public RieException(String message) {
        super(message);
    }

    public RieException(String message, Throwable cause) {
        super(message, cause);
    }

    public RieException(Throwable cause) {
        super(cause);
    }
}
