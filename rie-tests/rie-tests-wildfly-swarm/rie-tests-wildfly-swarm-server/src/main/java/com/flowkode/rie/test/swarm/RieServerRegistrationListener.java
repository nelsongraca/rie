package com.flowkode.rie.test.swarm;

import com.flowkode.rie.server.RieServerServlet;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Created by ngraca on 12/29/16.
 */
@WebListener
public class RieServerRegistrationListener implements ServletContextListener {

    @Override
    public void contextDestroyed(ServletContextEvent event) {
    }

    @Override
    public void contextInitialized(ServletContextEvent event) {
        event.getServletContext().addServlet("RieServerServlet", RieServerServlet.class).addMapping("/rie");
    }
}

