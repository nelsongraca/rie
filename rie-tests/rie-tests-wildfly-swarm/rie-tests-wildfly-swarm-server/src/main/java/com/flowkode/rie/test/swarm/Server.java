package com.flowkode.rie.test.swarm;


import org.wildfly.swarm.Swarm;
import org.wildfly.swarm.undertow.UndertowFraction;

public class Server {

    public static void main(String... args) throws Exception {
        new Swarm()
                .fraction(UndertowFraction.createDefaultFraction())
                .start()
                .deploy();
    }
}
