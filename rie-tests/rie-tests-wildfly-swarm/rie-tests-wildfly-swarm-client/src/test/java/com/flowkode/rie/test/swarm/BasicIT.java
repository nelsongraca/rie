package com.flowkode.rie.test.swarm;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.jboss.shrinkwrap.resolver.api.maven.MavenResolvedArtifact;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.wildfly.swarm.Swarm;
import org.wildfly.swarm.arquillian.CreateSwarm;
import org.wildfly.swarm.config.naming.Binding;
import org.wildfly.swarm.naming.NamingFraction;
import org.wildfly.swarm.undertow.UndertowFraction;

import javax.inject.Inject;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Created by ngraca on 12/29/16.
 */
@RunWith(Arquillian.class)
public class BasicIT {

    @Inject
    private ITest test;

    @Deployment
    public static WebArchive createDeployment() {
        final WebArchive webArchive = ShrinkWrap.create(WebArchive.class);

        MavenResolvedArtifact[] libs = Maven
                .configureResolver()
                .workOffline()
                .loadPomFromFile("pom.xml")
                .importRuntimeDependencies()
                .resolve()
                .withTransitivity()
                .asResolvedArtifact();

        for (MavenResolvedArtifact lib : libs) {
            webArchive.addAsLibrary(lib.asFile());
        }

        return webArchive.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @CreateSwarm
    public static Swarm createSwarm() throws Exception {
        return new Swarm()
                .fraction(UndertowFraction.createDefaultFraction())
                .fraction(new NamingFraction()
                                  .binding(new Binding("java:/rie/remoteServer")
                                                   .bindingType(Binding.BindingType.SIMPLE)
                                                   .value("http://localhost:28090/rie")
                                  )
                );
    }

    @Test
    public void doTest() {
        assertThat(test.getString(), is(equalTo("com.flowkode.rie.test.swarm.TestImpl")));
        assertThat(test.hello("John Doe"), is(equalTo("Hello John Doe")));

        SomePojo pojo = new SomePojo("John", 0L);

        SomePojo result;
        result = test.appendToString(pojo, "Doe");

        assertThat(result.getStr(), is(equalTo("JohnDoe")));
        assertThat(result.getLng(), is(equalTo(0L)));

        result = test.addToLong(result, 10L);

        assertThat(result.getStr(), is(equalTo("JohnDoe")));
        assertThat(result.getLng(), is(equalTo(10L)));
    }

    @Test(expected = RuntimeException.class)
    public void runtimeException() {
        test.runtimeException();
    }

    @Test(expected = Exception.class)
    public void exception() throws Exception {
        test.exception();
    }


}
