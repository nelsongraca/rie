package com.flowkode.rie.test.swarm;

import com.flowkode.rie.util.RieClient;

/**
 * Created by ngraca on 12/29/16.
 */
@RieClient
public interface ITest {

    String getString();

    String hello(String name);

    String runtimeException();

    String exception() throws Exception;

    SomePojo appendToString(SomePojo somePojo,String stringToAppend);

    SomePojo addToLong(SomePojo somePojo,Long amountToAdd);
}
