package com.flowkode.rie.test.springboot;

import java.io.Serializable;

/**
 * Created by ngraca on 1/12/17.
 */
public class SomePojo implements Serializable {

    private static final long serialVersionUID = 1L;

    private String str;

    private Long lng;

    public SomePojo(String str, Long lng) {
        this.str = str;
        this.lng = lng;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public Long getLng() {
        return lng;
    }

    public void setLng(Long lng) {
        this.lng = lng;
    }
}
