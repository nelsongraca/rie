package com.flowkode.rie.test.springboot;

import org.springframework.stereotype.Component;

/**
 * Created by ngraca on 12/29/16.
 */
@Component
public class TestImpl implements ITest {

    @Override
    public String getString() {
        return getClass().getName();
    }

    @Override
    public String hello(String name) {
        return "Hello " + name;
    }

    @Override
    public String runtimeException() {
        throw new RuntimeException("RUNTIME EXCEPTION!");
    }

    @Override
    public String exception() throws Exception {
        throw new Exception("EXCEPTION!");
    }

    @Override
    public SomePojo appendToString(SomePojo somePojo, String stringToAppend) {
        return new SomePojo(somePojo.getStr().concat(stringToAppend), somePojo.getLng());
    }

    @Override
    public SomePojo addToLong(SomePojo somePojo, Long amountToAdd) {
        return new SomePojo(somePojo.getStr(), somePojo.getLng() + amountToAdd);
    }
}
