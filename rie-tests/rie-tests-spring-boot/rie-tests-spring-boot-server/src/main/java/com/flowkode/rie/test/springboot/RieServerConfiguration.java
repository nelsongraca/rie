package com.flowkode.rie.test.springboot;

import com.flowkode.rie.server.RieServerServlet;
import com.flowkode.rie.server.SpringContextProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.DispatcherServletAutoConfiguration;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by ngraca on 12/29/16.
 */
@Configuration
public class RieServerConfiguration {

    @Bean
    public ServletRegistrationBean dispatchServletRegistration() {
        ServletRegistrationBean registration = new ServletRegistrationBean(new RieServerServlet(), "/rie");
        registration.setName(DispatcherServletAutoConfiguration.DEFAULT_DISPATCHER_SERVLET_REGISTRATION_BEAN_NAME);
        return registration;
    }

    @Bean
    public SpringContextProvider springContextProvider(@Autowired ApplicationContext applicationContext) {
        return new SpringContextProvider(applicationContext);
    }
}

