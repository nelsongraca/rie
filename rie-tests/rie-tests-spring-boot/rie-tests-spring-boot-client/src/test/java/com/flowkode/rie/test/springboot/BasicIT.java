package com.flowkode.rie.test.springboot;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Created by ngraca on 12/29/16.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = Client.class)
public class BasicIT {

    @Autowired
    private ITest test;

    @Test
    public void doTest() {
        assertThat(test.getString(), is(equalTo("com.flowkode.rie.test.springboot.TestImpl")));
        assertThat(test.hello("John Doe"), is(equalTo("Hello John Doe")));

        SomePojo pojo = new SomePojo("John", 0L);

        SomePojo result;
        result = test.appendToString(pojo, "Doe");

        assertThat(result.getStr(), is(equalTo("JohnDoe")));
        assertThat(result.getLng(), is(equalTo(0L)));

        result = test.addToLong(result, 10L);

        assertThat(result.getStr(), is(equalTo("JohnDoe")));
        assertThat(result.getLng(), is(equalTo(10L)));
    }

    @Test(expected = RuntimeException.class)
    public void runtimeException() {
        test.runtimeException();
    }

    @Test(expected = Exception.class)
    public void exception() throws Exception {
        test.exception();
    }


}
