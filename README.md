#WORK IN PROGRESS

#Only CDI is supported yet

#README

##Why?
In a micro-services scenario there is always the need for your services to communicate. It can be either asynchronous with JMS, you can write REST web-services for synchronous stuff.

Because it will require some amount of time to write those services and client code to consume them, this project was born, the idea is that you create an interface and its implementation, on the service side you will have both, on the client side you only need the interface and RIE will do the rest of the work of invoking the code on the service side.
This can be compared to remote EJB invocations.

##How to use

###Service Side (Server)

Add the following dependency maven to your project:
    
    <dependency>
        <groupId>com.flowkode.rie</groupId>
        <artifactId>rie-server</artifactId>
        <version>0.1.0-SNAPSHOT</version>
    </dependency>

And configure the `RieServerServlet` either with web.xml or any other preferred way.

###Client Side

Add the following dependency maven to your project:
    
    <dependency>
        <groupId>com.flowkode.rie</groupId>
        <artifactId>rie-client</artifactId>
        <version>0.1.0-SNAPSHOT</version>
    </dependency>

Configure the server endpoint and then just `@Inject` your interface, and you are ready to call your services

####Configure the endpoint

Right now the endpoint configuration can only be done using JNDI `java:/rie/remoteServer`


##License
This project is licensed under the Apache License, Version 2.0 - http://www.apache.org/licenses/LICENSE-2.0.txt