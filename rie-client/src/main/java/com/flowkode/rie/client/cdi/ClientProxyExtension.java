package com.flowkode.rie.client.cdi;

import com.flowkode.rie.util.RieClient;

import javax.enterprise.event.Observes;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.spi.*;
import javax.enterprise.util.AnnotationLiteral;
import javax.inject.Qualifier;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ClientProxyExtension implements Extension {

    private Map<Class<?>, Set<Annotation>> classes = new HashMap<>();

    public ClientProxyExtension() {
    }

    protected <X> void processAnnotatedType(@Observes ProcessAnnotatedType<X> processAnnotatedType) {
        AnnotatedType<X> annotatedType = processAnnotatedType.getAnnotatedType();
        Class<X> classType = annotatedType.getJavaClass();
        Annotation annotation = classType.getAnnotation(RieClient.class);
        if (annotation != null) {
            classes.put(classType, getQualifiers(classType));
        }
    }

    void afterBeanDiscovery(@Observes AfterBeanDiscovery afterBeanDiscovery, BeanManager beanManager) {
        for (Map.Entry<Class<?>, Set<Annotation>> entry : classes.entrySet()) {
            afterBeanDiscovery.addBean(new RieBean<>(entry.getKey(), entry.getValue()));
        }
    }

    private Set<Annotation> getQualifiers(final Class<?> type) {

        Set<Annotation> qualifiers = new HashSet<>();
        Annotation[] annotations = type.getAnnotations();
        for (Annotation annotation : annotations) {
            Class<? extends Annotation> annotationType = annotation.annotationType();
            if (annotationType.isAnnotationPresent(Qualifier.class)) {
                qualifiers.add(annotation);
            }
        }

        // Add @Default qualifier if no qualifier is specified.
        if (qualifiers.isEmpty()) {
            qualifiers.add(DefaultAnnotationLiteral.INSTANCE);
        }

        // Add @Any qualifier.
        qualifiers.add(AnyAnnotationLiteral.INSTANCE);
        return qualifiers;
    }

    @SuppressWarnings("all")
    static class DefaultAnnotationLiteral extends AnnotationLiteral<Default> implements Default {

        private static final long serialVersionUID = 511359421048623933L;

        private static final DefaultAnnotationLiteral INSTANCE = new DefaultAnnotationLiteral();
    }

    @SuppressWarnings("all")
    static class AnyAnnotationLiteral extends AnnotationLiteral<Any> implements Any {

        private static final long serialVersionUID = 7261821376671361463L;

        private static final AnyAnnotationLiteral INSTANCE = new AnyAnnotationLiteral();
    }
}
