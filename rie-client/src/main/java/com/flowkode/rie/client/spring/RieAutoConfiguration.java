package com.flowkode.rie.client.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by LoneWolf on 16/01/2017.
 */
@Configuration
public class RieAutoConfiguration {

    @Bean
    public RieProxyRegistrar rieProxyRegistrar() {
        return new RieProxyRegistrar();
    }
}
