package com.flowkode.rie.client.cdi;

import com.flowkode.rie.client.RieInvocationHandler;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Stereotype;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.InjectionPoint;
import java.lang.annotation.Annotation;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by ngraca on 1/4/17.
 */
public class RieBean<T> implements Bean<T> {

    private final Class<?> type;

    private final Set<Annotation> qualifiers;

    public RieBean(Class<?> type, Set<Annotation> qualifiers) {
        this.type = type;
        this.qualifiers = qualifiers;
    }

    @Override
    public Class<?> getBeanClass() {
        return type;
    }

    @Override
    public Set<InjectionPoint> getInjectionPoints() {
        return Collections.emptySet();
    }

    @Override
    public boolean isNullable() {
        return false;
    }

    @Override
    public T create(CreationalContext<T> creationalContext) {
        return (T) Proxy.newProxyInstance(
                getClass().getClassLoader(),
                new Class[]{type},
                new RieInvocationHandler(type, null)
        );
    }

    @Override
    public void destroy(T instance, CreationalContext<T> creationalContext) {
        creationalContext.release();
    }

    @Override
    public Set<Type> getTypes() {
        Set<Class> interfaces = new HashSet<>();
        interfaces.add(type);
        interfaces.addAll(Arrays.asList(type.getInterfaces()));
        return new HashSet<>(interfaces);
    }

    @Override
    public Set<Annotation> getQualifiers() {
        return new HashSet<>(qualifiers);
    }

    @Override
    public Class<? extends Annotation> getScope() {
        return ApplicationScoped.class;
    }

    @Override
    public String getName() {
        return type.getName();
    }

    @Override
    public Set<Class<? extends Annotation>> getStereotypes() {
        Set<Class<? extends Annotation>> stereotypes = new HashSet<Class<? extends Annotation>>();

        for (Annotation annotation : type.getAnnotations()) {
            Class<? extends Annotation> annotationType = annotation.annotationType();
            if (annotationType.isAnnotationPresent(Stereotype.class)) {
                stereotypes.add(annotationType);
            }
        }

        return stereotypes;
    }

    @Override
    public boolean isAlternative() {
        return type.isAnnotationPresent(Alternative.class);
    }
}
