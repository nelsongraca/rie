package com.flowkode.rie.client.spring;

import com.flowkode.rie.util.RieClient;
import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.ConstructorArgumentValues;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.AnnotationBeanNameGenerator;

import java.util.Set;

/**
 * Created by ngraca on 1/16/17.
 */
public class RieProxyRegistrar implements BeanDefinitionRegistryPostProcessor {

    private final Set<Class<?>> classes;

    private final AnnotationBeanNameGenerator generator = new AnnotationBeanNameGenerator();

    public RieProxyRegistrar() {
        classes = new Reflections(ClasspathHelper.forJavaClassPath()).getTypesAnnotatedWith(RieClient.class);
    }

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        for (Class<?> clazz : classes) {
            ConstructorArgumentValues cav = new ConstructorArgumentValues();
            cav.addGenericArgumentValue(clazz);

            registry.registerBeanDefinition(
                    generator.generateBeanName(new RootBeanDefinition(clazz), registry),
                    new RootBeanDefinition(RieClientFactory.class, cav, null)
            );
        }
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
    }
}
