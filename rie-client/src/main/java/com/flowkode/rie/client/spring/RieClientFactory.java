package com.flowkode.rie.client.spring;

import com.flowkode.rie.client.RieInvocationHandler;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Value;

import java.lang.reflect.Proxy;

/**
 * Created by ngraca on 1/16/17.
 */
public class RieClientFactory implements FactoryBean {

    private final Class clazz;

    @Value("${rie.remote.server}")
    private String remoteServer;

    public RieClientFactory(Class clazz) {
        this.clazz = clazz;
    }

    @Override
    public Object getObject() throws Exception {
        return Proxy.newProxyInstance(
                getClass().getClassLoader(),
                new Class[]{clazz},
                new RieInvocationHandler(clazz, remoteServer)
        );
    }

    @Override
    public Class<?> getObjectType() {
        return clazz;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
