package com.flowkode.rie.client;

import com.flowkode.rie.util.RieException;
import com.flowkode.rie.util.SerializationUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.NotSerializableException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ngraca on 1/4/17.
 */
public class RieInvocationHandler implements InvocationHandler {

    private static final Logger logger = LoggerFactory.getLogger(RieInvocationHandler.class);

    private final Class<?> clazz;

    private HttpClient client;

    private String remoteUrl;

    public RieInvocationHandler(Class<?> clazz, String remoteUrl) {
        this.clazz = clazz;
        this.remoteUrl = remoteUrl;
        client = HttpClientBuilder.create().build();
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //remote invocation
        String interfaceName = clazz.getName();
        String methodName = method.getName();
        final String arguments;

        try {
            arguments = SerializationUtils.serializeToBase64(args);
        }
        catch (NotSerializableException ex) {
            throw new RieException("One of your arguments is not serializable!", ex);
        }

        HttpPost httpPost = new HttpPost(getRemoteUrl());

        List<NameValuePair> parameters = new ArrayList<>();
        parameters.add(new BasicNameValuePair("interfaceName", interfaceName));
        parameters.add(new BasicNameValuePair("methodName", methodName));
        parameters.add(new BasicNameValuePair("arguments", arguments));

        httpPost.setEntity(new UrlEncodedFormEntity(parameters));
        HttpResponse result = client.execute(httpPost);

        final Object object = SerializationUtils.deserializeFromBase64(result.getEntity().getContent());
        if (object instanceof Throwable) {
            throw new RuntimeException((Throwable) object);
        }
        return object;
    }


    private String getRemoteUrl() {
        //do we have JNDI? try to get it from there
        try {
            return (String) new InitialContext().lookup("java:/rie/remoteServer");
        }
        catch (NamingException e) {
            logger.info("Failed to get remoteUrl from JNDI.");
        }
        //try system properties then
        final String property = System.getProperty("rie.remote.server");
        if (property != null) {
            return property;
        }
        //was it set here?
        if (remoteUrl != null) {
            return remoteUrl;
        }
        return null;
    }
}
